const emailAddress = 'mailto:alex@alexbarron.xyz';
const youtube = 'https://www.youtube.com/channel/UC7dUg3m9dD3nRqb5joq2crQ';
const linkedin = 'https://linkedin.com/in/abarron87';

export default emailAddress;
export { youtube, linkedin };