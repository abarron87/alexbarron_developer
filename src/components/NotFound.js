import React from 'react';
import { Typography } from '@material-ui/core';

const NotFound = () => {
  return <Typography variant="h2">Page not found.</Typography>;
}

export default NotFound;