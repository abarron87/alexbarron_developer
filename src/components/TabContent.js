import React, { Fragment } from 'react';

const TabContent = (props) => {
  return (
    <Fragment>
      {props.children}
    </Fragment>
  );
}

export default TabContent;